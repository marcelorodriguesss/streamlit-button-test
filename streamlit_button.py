import streamlit as st

st.sidebar.title('CLIMATE FORECAST')

model_choice = st.sidebar.selectbox('MODELS:', ['CFSv2', 'GFDL'])

var_choice = st.sidebar.selectbox('VARS:', ['PCP', 'TEMP'])

submit = st.sidebar.button('submit')

if submit:
    if model_choice == 'CFSv2' and var_choice == 'PCP':
        fig = 'https://www.cpc.ncep.noaa.gov/products/NMME/current/images/CFSv2_ensemble_prate_season1.png'
    elif model_choice == 'CFSv2' and var_choice == 'TEMP':
        fig = 'https://www.cpc.ncep.noaa.gov/products/NMME/current/images/CFSv2_ensemble_tmp2m_season1.png'
    elif model_choice == 'GFDL' and var_choice == 'PCP':
        fig = 'https://www.cpc.ncep.noaa.gov/products/NMME/current/images/GFDL_FLOR_ensemble_prate_season1.png'
    else:
        fig = 'https://www.cpc.ncep.noaa.gov/products/NMME/current/images/GFDL_FLOR_ensemble_tmp2m_season1.png'
    st.image(fig, format='png')

